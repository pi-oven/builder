#!/bin/sh

PI_DIR=/pi-oven
ANSIBLE_DIR=${PI_DIR}/ansible
ATTEMPTS_FILE="${PI_DIR}/.attempts"
DATA_DIR=${PI_DIR}/data
LOG_FILE=/var/log/pi-oven.log
MAX_ATTEMPTS=5
WIFI_CONFIG_FILE=/etc/wpa_supplicant/wpa_supplicant.conf

# shellcheck source=./src/data/settings.sh
. ${DATA_DIR}/settings.sh

if test -f "${LOG_FILE}"; then
  mv ${LOG_FILE} "${LOG_FILE}-$(date +%s)"
fi

log() {
  echo "[$(date --iso-8601=seconds)] $1"
}

if [ -z "${LOG_TO_CONSOLE}" ]
then
  exec 3>&1 4>&2
  trap 'exec 2>&4 1>&3' 0 1 2 3
  exec 1>${LOG_FILE} 2>&1
fi

set -e

log "Starting script"

log "Enable SSH"
systemctl enable ssh
systemctl start ssh

# Setup WiFi
if [ -n "${PI_WIFI_SSID}" ]
then
  log "Setup WiFi"

  if ! grep -q country "${WIFI_CONFIG_FILE}"; then
    log "Applying WiFi country"
    echo country="${PI_WIFI_COUNTRY}" >> "${WIFI_CONFIG_FILE}"
  fi

  if ! grep -q network "${WIFI_CONFIG_FILE}"; then
    log "WiFi is unconfigured"

    echo "network={
  ssid=\"${PI_WIFI_SSID}\"
  psk=\"${PI_WIFI_PSK}\"
}" >> "${WIFI_CONFIG_FILE}"

  reboot
  else
    log "WiFi is configured - skipping"
  fi
fi

{
  # Install Ansible
  log "Installing Ansible"
  apt-get update
  apt-get install -y ansible

  # Generate Ansible variables
  log "Generating Ansible variables"

  randomWord1=$(shuf ${DATA_DIR}/words.txt -n 1 | sed -e "s/\s/-/g")
  randomWord2=$(shuf ${DATA_DIR}/words.txt -n 1 | sed -e "s/\s/-/g")
  export PI_CONFIG_HOSTNAME="${PI_HOSTNAME}-${randomWord1}-${randomWord2}"

  log "Listing Pi Oven variables"
  env | grep PI_

  # Run Ansible
  log "Running ansible playbook"
  ansible-playbook ${ANSIBLE_DIR}/pi-oven.yml \
    --extra-vars "auth_keys_github=${PI_AUTH_KEYS_GITHUB}" \
    --extra-vars "auth_keys_gitlab=${PI_AUTH_KEYS_GITLAB}" \
    --extra-vars "docker_install=${PI_DOCKER_INSTALL}" \
    --extra-vars "pi_gpu_mem=${PI_GPU_MEM}" \
    --extra-vars "pi_hostname=${PI_CONFIG_HOSTNAME}" \
    --extra-vars "pi_password=${PI_PASSWORD}" \
    --extra-vars "pi_username=${PI_USERNAME}"
} || {
  ATTEMPTS=$(cat ${ATTEMPTS_FILE} || echo "0")
  log "Ansible playbook failed - attempts ${ATTEMPTS}"

  ATTEMPTS=$((ATTEMPTS+1))
  echo "${ATTEMPTS}" > ${ATTEMPTS_FILE}

  if [ "${ATTEMPTS}" -le ${MAX_ATTEMPTS} ]
  then
    log "Further attempts possible - rebooting"
    reboot
  else
    log "Exceeded max number of attempts (${MAX_ATTEMPTS}) - failing"
    exit 1
  fi
}

log "Removing installation dependencies"
apt-get purge -y ansible
apt-get -y autoremove

log "Removing ${PI_DIR} directory"
rm -Rf ${PI_DIR}

# Failsafe
log "Removing script ${0}"
rm -- "$0" || true

log "Script finished"
reboot
