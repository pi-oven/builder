#!/bin/sh

SCRIPT_DIR="$( cd "$(dirname "$0")" || exit ; pwd -P )"
ROOT_DIR="${SCRIPT_DIR}/.."
INIT_FILE="/etc/rc.local"
TARGET_FILE="/pi-oven/runner.sh"

# Set the first boot script
if ! (grep -q "${TARGET_FILE}" "${ROOT_DIR}${INIT_FILE}"); then
  echo "Inserting the runner script in ${INIT_FILE}"

  # Remove last line - this will be "exit 0"
  sed -i '$ d' "${ROOT_DIR}${INIT_FILE}"

  # Add in the new content
  {
    echo "if [ -f ${TARGET_FILE} ]; then"
    echo "  echo 'Setting up the Pi'"
    echo "  sh ${TARGET_FILE}"
    echo "fi"
    echo ""
    echo "exit 0"
  } >> "${ROOT_DIR}${INIT_FILE}"
else
  echo "Runner script execution already in ${INIT_FILE}"
fi
