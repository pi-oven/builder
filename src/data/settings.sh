#!/bin/sh

# This file will be different on each installation - this is a basic working example

# Ansible variables
export PI_AUTH_KEYS_GITHUB="" # Can be blank
export PI_AUTH_KEYS_GITLAB="" # Can be blank
export PI_DOCKER_INSTALL="true" # Boolean
export PI_GPU_MEM="16" # Can be blank
export PI_HOSTNAME="hostname" # This will appended with two random words
export PI_PASSWORD="q1w2e3r4" # This will be escaped by ansible
export PI_USERNAME="username"

# Non-Ansible variables
export PI_WIFI_COUNTRY="GB"
export PI_WIFI_SSID="wifi_ssd"
export PI_WIFI_PSK="wifi_passwd"
