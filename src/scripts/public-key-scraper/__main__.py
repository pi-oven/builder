import argparse
import datetime
import os
from sources import sources

start_time = datetime.datetime.utcnow().replace(microsecond=0).isoformat()

parser = argparse.ArgumentParser(
    description='Get public keys from external sources and store in authorized_keys format',
    prog='Public Key Scraper',
    add_help=True
)

for Source in sources:
    parser.add_argument(
        '--' + Source.id,
        dest=Source.id,
        help='Add keys for a ' + Source.name + ' user',
        action='append',
        default=[],
        type=str
    )

parser.add_argument(
    '--target',
    dest='target',
    help='File to store your keys',
    default=os.path.expanduser('~/.ssh/authorized_keys'),
    type=str
)

args = parser.parse_args()


def valid_input(source):
    input_data = args.__dict__.get(source.id)

    filtered_input = list(filter(lambda x: x, input_data))

    return len(filtered_input) > 0


# Check at least one source is set
sources_set = any(valid_input(source) for source in sources)
assert sources_set, 'You must set at least one source for public keys'

target = os.path.expanduser(args.target)

# Let's go - get the keys
keys = []
for Source in sources:
    for value in args.__dict__.get(Source.id):
        if value:
            downloader = Source(value)
            keys = keys + downloader.get_keys()

dedupe_keys = list(dict.fromkeys(keys))

target_dir = os.path.dirname(target)
if not os.path.isdir(target_dir):
    os.makedirs(target_dir, 0755)

target_file = open(target, 'w+')
target_file.writelines('# Generated ' + start_time + '\n')
for key in dedupe_keys:
    target_file.write(key + '\n')
target_file.close()

print('Imported {} key(s)'.format(len(dedupe_keys)))
