import downloader
import requests


class GitLab(downloader.Downloader):
    id = 'gitlab'
    name = 'GitLab'

    def get_keys(self):
        user_id = str(self.get_user_id())

        url = 'https://gitlab.com/api/v4/users/' + user_id + '/keys'
        data = requests.get(url=url)

        if data.status_code != 200:
            raise Exception('Unknown GitLab user: ' + self.username)

        return list(map(lambda x: x['key'], data.json()))

    def get_user_id(self):
        url = 'https://gitlab.com/api/v4/users?username=' + self.username
        data = requests.get(url=url)
        response = data.json()

        if data.status_code != 200:
            raise Exception('Unknown GitLab user: ' + self.username)

        if response and len(response) > 0 and response[0]['id']:
            return response[0]['id']

        raise Exception('Unknown GitLab user: ' + self.username)
