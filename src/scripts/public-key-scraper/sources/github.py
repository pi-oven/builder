import downloader
import requests


class GitHub(downloader.Downloader):
    id = 'github'
    name = 'GitHub'

    def get_keys(self):
        url = 'https://api.github.com/users/' + self.username + '/keys'
        data = requests.get(url=url)

        if data.status_code != 200:
            raise Exception('Unknown GitHub user: ' + self.username)

        return list(map(lambda x: x['key'], data.json()))
