#!/bin/sh

set -e

SCRIPT_DIR="$( cd "$(dirname "$0")" || exit ; pwd -P )"
CURRENT_FILE="${0}"
ISO_PATH=""
SETTINGS_FILE=""
USE_LOCAL="false"
PI_OVEN_URL="https://releases.pi-oven.com/pi-oven.tar.gz"
BOOT_DIR="/media/rpi_boot"
ROOT_DIR="/media/rpi_root"

unmount_dirs () {
  umount ${BOOT_DIR} || true
  umount ${ROOT_DIR} || true
}

if [ "${#}" = "0" ];
then
  # No arguments - rerun this script with "-h" flag
  ${0} -h
  exit 1
fi

# Parse options to the `pip` command
while getopts ":hi:ls:" opt; do
  case ${opt} in
    h )
      echo "Usage:"
      echo "  ${CURRENT_FILE} [options]"
      echo ""
      echo "Options:"
      echo "  -h            Display this help message."
      echo "  -i <path>     Path to Raspbian image."
      echo "  -l            Use the local Pi Oven files rather than downloading from the web [false]"
      echo "  -s <path>     Path to settings file"
      exit 0
      ;;
    i ) ISO_PATH="${OPTARG}" ;;
    l ) USE_LOCAL="true" ;;
    s ) SETTINGS_FILE="${OPTARG}" ;;
    \? )
      echo "Invalid Option: -$OPTARG" 1>&2
      ${0} -h
      exit 1
    ;;
  esac
done

if [ "${ISO_PATH}" = "" ]
then
  echo "You must select the ISO file"
  exit 1
fi

if [ ! -f "${ISO_PATH}" ]
then
  echo "Cannot access ISO: ${ISO_PATH}"
  exit 1
fi

if [ "${SETTINGS_FILE}" = "" ]
then
  echo "You must select the settings file"
  exit 1
fi

if [ ! -f "${SETTINGS_FILE}" ]
then
  echo "Cannot access settings: ${SETTINGS_FILE}"
  exit 1
fi

# Create directories to mount the images to
mkdir -p "${BOOT_DIR}" "${ROOT_DIR}"

unmount_dirs
kpartx -d "${ISO_PATH}" || true

LOOP=$(kpartx -sva "${ISO_PATH}")

MOUNT_POINTS=$(echo "${LOOP}" | grep -ioE 'loop(\w+)')

for line in ${MOUNT_POINTS}; do
  case ${line} in
    *p1 )
      echo "Mounting boot partition"
      mount "/dev/mapper/${line}" ${BOOT_DIR} -o ro --rw
      ;;
    *p2 )
      echo "Mounting root partititon"
      mount "/dev/mapper/${line}" ${ROOT_DIR} -o ro --rw
      ;;
    * )
      echo "Unknown mount point in image: ${ISO_PATH}"
      exit 1
      ;;
  esac
done

sleep 1

# Download/copy the pi-oven files to the card
rm -Rf ${BOOT_DIR}/pi-oven

if [ "${USE_LOCAL}" = "true" ]
then
  echo "Copying local version of Pi Oven"
  cp -rf "${SCRIPT_DIR}" ${ROOT_DIR}/pi-oven
else
  echo "Downloading Pi Oven from ${PI_OVEN_URL}"
  wget ${PI_OVEN_URL} -O ${ROOT_DIR}/tmp/pi-oven.tar.gz
  tar -zxvf ${ROOT_DIR}/tmp/pi-oven.tar.gz -C ${ROOT_DIR}
fi

# Copy settings file and tell the Pi to run the setup on boot
echo "Copying settings file"
cp "${SETTINGS_FILE}" ${ROOT_DIR}/pi-oven/data/settings.sh

echo "Running the Pi Oven setup on boot"
sh ${ROOT_DIR}/pi-oven/init.sh

## Clean up after ourselves
echo "Unmounting ISO"
unmount_dirs
kpartx -d "${ISO_PATH}"

echo "ISO - flash this image to a card and boot your Pi"
