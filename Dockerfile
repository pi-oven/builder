FROM ubuntu:bionic
WORKDIR /opt/app
ADD ./src .
RUN apt-get update \
  && apt-get install -y curl kpartx
VOLUME [ "/iso" ]
ENTRYPOINT [ "sh", "/opt/app/build-iso.sh" ]
