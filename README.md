# builder

Application to build headless Raspberry Pis

# Building You Pi Oven Image

> This is the step-by-step manual process. If you want to automate it
> (recommended), use the [Docker](#docker) image detailed below

1. Download a [Raspbian](https://www.raspberrypi.org/downloads/raspbian)
image (it's recommended to use the Lite image, although it will work with any
available version).
2. Mount the root partition of the image.
3. Download a release of
[Pi Oven](https://gitlab.com/pi-oven/builder/-/releases).
4. Copy the `/src` directory to `/pi-oven` on the root partition of the
mounted image.
5. Copy your [settings](#settings) file to `/pi-oven/data/settings.sh`.
6. Run the `sudo sh /pi-oven/init.sh` command on the mounted image. 
7. Unmount the image
8. Now you've got the image created, flash it to your memory card with 
[Etcher](https://www.balena.io/etcher).

When you power-up the Pi, which will take about 5 minutes, you will be able
to connect to it with the credentials provided.

## Debugging

Logs are stored to `/var/log/pi-oven`. There may be additional log files with
the timestamp appended.

# Shell Scripts

## build-iso.sh

This is used to automate the building of the ISO from a base Raspbian image.

```shell script
Usage:
  ./build-iso.sh [options]

Options:
  -h            Display this help message.
  -i <path>     Path to Raspbian image.
  -l            Use the local Pi Oven files rather than downloading from the web [false]
  -s <path>     Path to settings file
```

- `-i <path>`: This is the path to Raspbian image.
- `-l`: Use the local Pi Oven files rather than downloading from the web. The
default is `false`.
- `-s <path>`: This is the path to the settings file. This can be generated
from [Pi-Oven.com](https://pi-oven.com) or by creating your [own](#settings).  

## init.sh

This is to be run once the files are copied to your Pi's memory card. This
amends the `/etc/rc.local` file to tell the Pi to load the Pi-Oven runner.sh
at boot (if it's present).

This is the recommended way of getting a Pi to run a script on boot.

## runner.sh

This is the runner script that is used to configure the Raspberry Pi.

# Docker

This Docker image exists to run the `build-iso.sh` file. It is the Ubuntu
image with [kpartx](https://linux.die.net/man/8/kpartx) installed.

> It doesn't use the Alpine image as the version of 
> [mount](https://linux.die.net/man/8/mount) doesn't allow for mounting a 
> image with writable permission.

This loads the [build-iso.sh](#build-isosh) script as an 
[entrypoint](https://docs.docker.com/engine/reference/builder/#entrypoint)
meaning the Docker image takes the same arguments are the script.

```shell script
docker run \
  -it \
  --rm \
  -v /path/to/raspbian.img:/iso/raspbian.img \
  -v /path/to/settings.sh:/iso/settings.sh \
  --privileged \
  registry.gitlab.com/pi-oven/builder \
  -i /iso/raspbian.img \
  -s /iso/settings.sh \
  -l
```

# Settings

```shell script
#!/bin/sh

# Ansible variables
export PI_AUTH_KEYS_GITHUB="" # Can be blank
export PI_AUTH_KEYS_GITLAB="" # Can be blank
export PI_DOCKER_INSTALL="true" # Boolean
export PI_GPU_MEM="16" # Can be blank
export PI_HOSTNAME="hostname" # This will appended with two random words
export PI_PASSWORD="q1w2e3r4" # This will be escaped by ansible
export PI_USERNAME="username"

# Non-Ansible variables
export PI_WIFI_COUNTRY="GB"
export PI_WIFI_SSID="wifi_ssd"
export PI_WIFI_PSK="wifi_passwd"
```
